/*JS Scorpio*/

var nav = jQuery.browser;

if (nav.chrome) {
    jQuery(document).ready(function () {
        jQuery(".mobile-menu").css({"background-color": "#000000", "color": "#FFFFFF", "float": "right", "padding-left": "28px", "padding-bottom": "5px"});
        jQuery(".mobile-menu > i").css({"margin-right": "28px"});
    });
} else if (nav.safari) {
	if (sessionStorage.getItem("key")) {
	    jQuery(document).ready(function () {
	        jQuery("button.slick-next.slick-arrow.fa.fa-angle-right").css("height", "70px");
	        jQuery(".mobile-menu").css({"background-color": "#000000", "padding-top": "11%", "color": "#FFFFFF", "padding-right": "30px", "padding-left": "30px", "padding-bottom": "26px"});
	    });
	} else {
		sessionStorage.setItem("key", true);
	    jQuery(document).ready(function () {
	        jQuery("button.slick-next.slick-arrow.fa.fa-angle-right").css("height", "70px");
	        jQuery(".mobile-menu").css({"background-color": "#000000", "padding-top": "11%", "color": "#FFFFFF", "padding-right": "30px", "padding-left": "30px", "padding-bottom": "28px"});
	    });
	}
} else {
    jQuery(document).ready(function () {
        jQuery(".mobile-menu").css({"background-color": "#000000", "padding-bottom": "5px", "padding-top": "11%", "color": "#FFFFFF", "padding-right": "30px", "padding-left": "30px"});
    });
}