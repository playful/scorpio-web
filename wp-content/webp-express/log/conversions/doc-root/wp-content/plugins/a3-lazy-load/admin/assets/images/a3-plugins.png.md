WebP Express 0.14.21. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2019-07-10 18:20:47

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.5
- Server software: LiteSpeed

Stack converter ignited
Destination folder does not exist. Creating folder: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/a3-lazy-load/admin/assets/images

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/a3-lazy-load/admin/assets/images/a3-plugins.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/a3-lazy-load/admin/assets/images/a3-plugins.png.webp
- log-call-arguments: true
- converters: (array of 4 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 80
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: vips* 

**Error: ** **Required Vips extension is not available.** 
Required Vips extension is not available.
vips failed in 0 ms

*Trying: gd* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/a3-lazy-load/admin/assets/images/a3-plugins.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/a3-lazy-load/admin/assets/images/a3-plugins.png.webp
- log-call-arguments: true
- quality: 85
- skip: false

The following options have not been explicitly set, so using the following defaults:
- default-quality: 85
- max-quality: 85

The following options were supplied but are ignored because they are not supported by this converter:
- alpha-quality
- encoding
- metadata
- near-lossless
------------

GD Version: bundled (2.1.0 compatible)
image is true color
Quality: 85. 
gd succeeded :)

Converted image in 13 ms, reducing file size with 57% (went from 3014 bytes to 1306 bytes)
